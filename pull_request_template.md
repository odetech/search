Test plan - (Please fill in how you tested your changes)

<!-- Please make sure your submission complies with our [Development](https://gitlab.com/odetech/document/wiki/Presto-Development-Guidelines#development), [Formatting](https://gitlab.com/odetech/document/wiki/Presto-Development-Guidelines#formatting), and [Commit Message](https://gitlab.com/prestodb/presto/wiki/Review-and-Commit-guidelines#commit-formatting-and-pull-requests) guidelines. Don't forget to follow our [attribution guidelines](https://gitlab.com/prestodb/presto/wiki/Review-and-Commit-guidelines#attribution) for any code copied from other projects. -->
<!-- 
Fill in the release notes towards the bottom of the PR description.
See [Release Notes Guidelines](https://gitlab.com/prestodb/presto/wiki/Release-Notes-Guidelines) for details. -->

```
== RELEASE NOTES ==

General Changes
* ...
* ...

Hive Changes
* ...
* ...
```

If release note is NOT required, use:

```
== NO RELEASE NOTE ==
```