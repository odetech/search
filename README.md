# Official Site of @odecloud/search

[![pipeline status](https://gitlab.com/odetech/search/badges/main/pipeline.svg)](https://gitlab.com/odetech/search/-/commits/main)[![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)](https://opensource.org/licenses/MIT)

**Summary** ✨

Package contain helper functions currently utilized by a varieties of apps developed @odecloud.

# Requirements

The package currently depend on these versions of *Node* and *NPM*.
```
$ node --version
v0.10.24

$ npm --version
1.3.21
```
# Installation

Make you are currently in a *Node* project that contains a valid `package.json` file. Run the following below to download the package.
```
npm install @odecloud/search
```

# Usage Example

The example below assumes that your current project is using the latest ES6.
```
import OdeCloud from '@odecloud/search';

// Instantiate the service
const odecloud = new OdeSearch({
  endpoint: "http://localhost:3000",
});

// Login to OdeCloud
// If you don't have an active OdeCloud's user account,
// then you can create one for free here: https://odecloud.app/signup
await odecloud.authenticate({
  email: "",    // your email
  password: "", // your password
});

const results = await odesearch.general.post({
  "search": "JVC project",
  "pageNumber": 1,
  "limit": 15, 
}); // return a list of search results matching the `search` value
```

# Local Testing/Development

You can download the package locally to your machine using:
```
$ git clone https://gitlab.com/odetech/search.git @odecloud/search
```

## Publishing to NPM

1. On the root of the directory, open up **package.json**
2. Bump the __version__ by the following guideline:
    - Our version numbering follows **Major.Minor.Patch** (e.g. 2.10.1)
        - **Major**: Stable release.
        - **Minor**: Incremental changes--including new apis, remove apis, or change of behavior of apis.
        - **Patch**: Small efficient changes--including a fixed to a bug.
    - **Note**: in regards to Patch if the old functionality was always erroneous, then it will be considered a Patch.
3. Publish a new tag on the repository by going to https://gitlab.com/odetech/search/-/tags/new.
    - **Note**: make sure that the "Tag Name" is an exact match to the version inside `package.json` on step #2.
    - In regards to the "Release notes": we encourage detail and consistent format in order for other developers to understand the new version update.

## Commands
- `npm test` - Run tests with linting and coverage results.
- `npm type-check` - TypeScript checking
- `type-check:watch` - TypeScript checking and watching
- `npm run build` - Babel will transpile ES6 and TypeScript => ES5 and minify the code.

# License

MIT © OdeCloud, Inc
