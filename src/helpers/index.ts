import ky from 'ky';

export const getRequestOptions = (
  searchParams: any,
  authToken: string,
  createAuthorizationHeader: any,
  userId: string,
) => {
  const options: any = {
    // responseType: 'json',
  };

  if (authToken) {
    options.headers = createAuthorizationHeader(authToken, userId);
  }

  if (searchParams) {
    options.searchParams = searchParams;
  }

  return options;
};

export const createAuthorizationHeader = (authToken: string, userId: string) => {
  const headers: any = {
    'Content-Type': 'application/json',
  };

  if (authToken) {
    headers['X-Auth-Token'] = authToken;
    headers['X-User-Id'] = userId;
    return headers;
  }
  return headers;
};

export const authentication = async (credentials: any, getEndpoint: any, setAuthToken: any, setUserId: any) => {
  if (!('password' in credentials)) {
    return;
  }

  if (!('username' in credentials) && !('email' in credentials)) {
    return;
  }

  const requestBody = credentials;

  try {
    const response: any = await ky.post(
      getEndpoint('login'),
      {
        json: requestBody,
      },
    ).json();

    if ('authToken' in response) {
      setAuthToken(response.authToken);
    }

    if ('userId' in response) {
      setUserId(response.userId);
    }

    return response;
  } catch (e) {
    // handle error
    throw e;
  }
};
