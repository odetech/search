import {
  GeneralTypes,
} from '../api';

/**************************
 ***** API TYPES ***
 **************************/

export {
  GeneralTypes,
};
