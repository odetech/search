import {
  createAuthorizationHeader, getRequestOptions,
} from './helpers';
import {
  general,
} from './api';

class OdeSearch {
  _authToken = '';
  _endpoint = '';
  userId = '';

  constructor(data: any) {
    this._authToken = '';
    this._endpoint = '';
    this.userId = '';

    if ('endpoint' in data) {
      this._endpoint = data['endpoint'];
    } else {
      throw Error("'endPoint' is required. Example: endpoint='https://search.odecloud.app/api/v1");
    }

    if ('authToken' in data) {
      this._authToken = data['authToken'];
    } else {
      throw Error("'authToken' is required. Example: authToken='string'");
    }

    if ('userId' in data) {
      this.userId = data['userId'];
    } else {
      throw Error("'userId' is required. Example: userId='string'");
    }
  }

  _getRequestOptions = (searchParams: any) => getRequestOptions(
    searchParams,
    this._authToken,
    createAuthorizationHeader,
    this.userId,
  );

  getEndpoint = (requestAPi: string) => {
    return `${this._endpoint}/${requestAPi}`;
  };

  /**************************
   ****** General API ******
   **************************/

  general = { ...general(this._getRequestOptions, this.getEndpoint) };
}

export default OdeSearch;
