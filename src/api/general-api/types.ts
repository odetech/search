type GeneralTypes = {
  limit: number,
  count: number,
  data: any[],
  pageNumber: number,
  hasNextPage: boolean,
};

export default GeneralTypes;
