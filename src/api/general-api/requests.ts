import {
  post,
  // get,
  // patch,
  // deleteRequest,
} from '../../base-requests';

export const requests = (getRequestOptions: any, getEndpoint: any) => ({
  post: async(payload: {
    search: string,
    pageNumber?: number,
    limit?: number,
  }) => {
    try {
      const data = await post(
        getRequestOptions,
        getEndpoint('general'),
        payload,
      );
      return data;
    } catch (e) {
      throw e;
    }
  },
});
