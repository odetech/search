import { expect } from 'chai';
import { General } from './model';


describe('General API', () => {
  const MockGeneral = () => new General({
    limit: 10,
    count: 10,
    data: [],
    pageNumber: 1,
    hasNextPage: false,
  });

  it('checks basic', () => {
    const general = MockGeneral();

    expect(general.limit).to.equal(10);
    expect(general.count).to.equal(10);
    expect(general.data.length).to.equal(0);
    expect(general.pageNumber).to.equal(1);
    expect(general.hasNextPage).to.equal(false);
  });

  describe('payload()', () => {
    const general = MockGeneral();

    it('checks getPostPayload()', () => {
      const payload = general.payload().getPostPayload(
        'string',
        1,
        10,
      );

      expect(payload.search).to.equal(payload.search);
      expect(payload.pageNumber).to.equal(payload.pageNumber);
      expect(payload.limit).to.equal(payload.limit);
    });
  });
});
