import GeneralTypes from './types';

export class General implements GeneralTypes {
  // `limit` of data points the backend should return back
  limit = 50;
  // # of data points actually available
  count = 0;
  // TODO: Write a separate model for this
  data = [];
  pageNumber = 1;   
  hasNextPage = false;

  constructor(data: any) {
    const keys = Object.keys(this);
    keys.forEach((key: string) => {
      if (data[key] !== undefined) {
        (this as any)[key] = data[key];
      }
    });
  }

  payload = () => {
    return ({
      getPostPayload: (search: string, pageNumber: number, limit: number) => {
        const payload = {
          search,
          pageNumber,
          limit,
        };
        return payload;
      },
    });
  };
}
