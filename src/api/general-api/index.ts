import { requests } from './requests';
import { General } from './model';
import GeneralTypes from './types';

const general = (getRequestOptions: any, getEndpoint: any) => {
  return ({
    model: General,
    payload: { ...new General({}).payload() },
    ...requests(getRequestOptions, getEndpoint),
  })
}

export {
  GeneralTypes,
};

export default general;
